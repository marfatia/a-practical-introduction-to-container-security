# Build and Deploy 

## On OpenShift

```
$ git clone https://gitlab.com/2020-summit-labs/a-practical-introduction-to-container-security.git

$ cd a-practical-introduction-to-container-security

$ oc new-project containersecuritylab

$ oc process -f build-template.yaml -p NAME="bookbag" -p GIT_REPO="https://gitlab.com/2020-summit-labs/a-practical-introduction-to-container-security.git" | oc apply -f -

$ oc start-build bookbag --follow

$ oc process -f deploy-template.yaml -p NAME="bookbag" -p IMAGE_STREAM_NAME="bookbag" -p WORKSHOP_VARS="$(cat workshop-vars.json)" | oc apply -f -
```

To update the WORKSHOP_VARS.

```
$ oc set env dc/bookbag WORKSHOP_VARS="{\"student_ssh_password\":\"mypassword\",\"guid\":\"summit\",\"student_ssh_command\":\"ssh lab-user@ec2-13-55-71-205.ap-southeast-2.compute.amazonaws.com\"}"
```